from datetime import datetime
from dateutil import tz

from pynwb import NWBFile, NWBHDF5IO
from ndx_odor_metadata import OdorMetaData

odor_table = OdorMetaData(name='odor_table', description='an odor table')

odor_table.add_stimulus(
    pubchem_id = 7662.0,
    stim_name = "3-Phenylpropyl isobutyrate",
    raw_id = 3,
    stim_id = 1,
    stim_types = "odor,legit",
    chemical_dilution_type='vaporized',
    chemical_concentration = 0.01,
    chemical_concentration_unit='%',
    chemical_solvent = "Mineral Oil",
    chemical_provider = "Sigma",
    stim_description = "Legit odor stimulus #1",
)


odor_table.add_stimulus(
    stim_name = "my sound",
    raw_id = 2,
    stim_id = 20,
    stim_types = "sound",
    stim_description = "lalaland",
)

nwbfile = NWBFile(
    session_description='abc',
    identifier='xyz',
    session_start_time= datetime(2018, 1, 2, 3, 4, 5, tzinfo=tz.gettz("US/Pacific"))
)

nwbfile.add_acquisition(odor_table)

with NWBHDF5IO('test.nwb', mode='w') as io:
    io.write(nwbfile)


nwbfile_reload_io = NWBHDF5IO('test.nwb', mode='r', load_namespaces=True)
nwbfile_reload = nwbfile_reload_io.read()
print(nwbfile_reload.acquisition['odor_table'].to_dataframe())
nwbfile_reload_io.close()
