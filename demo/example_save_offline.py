import pandas as pd
from copy import deepcopy

from datetime import datetime
from dateutil import tz

from pynwb import NWBFile, NWBHDF5IO
from ndx_odor_metadata import OdorMetaData, utils


# First read and online obtain the necessary valid pubchem ID
odor_df = pd.read_csv('odor-table-sample.csv', index_col=None)
valid_chem = odor_df.query('~pubchem_id.isnull()')\
    .filter(['pubchem_id', 'stim_name'])\
    .drop_duplicates()\
    .reset_index(drop=True)

assert not any(valid_chem['pubchem_id'].duplicated()),\
    'Found duplicated Pubchem ID. Please check `stim_name` spellings for stimulus with same `pubchem_id`'

# Query then save to an offline file to use as source for later
pubchem_offline_path = 'pubchem-offline.csv'
utils.save_pubchem_requests(
    valid_chem['pubchem_id'],
    valid_chem['stim_name'],
    pubchem_offline_path,
    warn_notfound_synonym=False # the warnings can be suppressed with this, also affect `validation_details`
)

# Create table from the offline file
odor_table = OdorMetaData(name='odor_table', description='an odor table')
for _, row in odor_df.iterrows():
    row = deepcopy(row)
    odor_table.add_stimulus(
        **row,
        source=pubchem_offline_path  # notice the addition of this
    )


# Create file and add table to
nwbfile = NWBFile(
    session_description='abc',
    identifier='xyz',
    session_start_time= datetime(2018, 1, 2, 3, 4, 5, tzinfo=tz.gettz("US/Pacific"))
)

nwbfile.add_acquisition(odor_table)

with NWBHDF5IO('test.nwb', mode='w') as io:
    io.write(nwbfile)

# Reload and read
nwbfile_reload_io = NWBHDF5IO('test.nwb', mode='r', load_namespaces=True)
nwbfile_reload = nwbfile_reload_io.read()
print(nwbfile_reload.acquisition['odor_table'].to_dataframe())
nwbfile_reload_io.close()
