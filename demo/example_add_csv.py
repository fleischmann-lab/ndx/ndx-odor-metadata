import pandas as pd
from copy import deepcopy

from datetime import datetime
from dateutil import tz

from pynwb import NWBFile, NWBHDF5IO
from ndx_odor_metadata import OdorMetaData


odor_df = pd.read_csv('odor-table-sample.csv', index_col=None)

odor_table = OdorMetaData(name='odor_table', description='an odor table')
for _, row in odor_df.iterrows():
    row = deepcopy(row)
    odor_table.add_stimulus(**row)


nwbfile = NWBFile(
    session_description='abc',
    identifier='xyz',
    session_start_time= datetime(2018, 1, 2, 3, 4, 5, tzinfo=tz.gettz("US/Pacific"))
)

nwbfile.add_acquisition(odor_table)

with NWBHDF5IO('test.nwb', mode='w') as io:
    io.write(nwbfile)


nwbfile_reload_io = NWBHDF5IO('test.nwb', mode='r', load_namespaces=True)
nwbfile_reload = nwbfile_reload_io.read()
print(nwbfile_reload.acquisition['odor_table'].to_dataframe())
nwbfile_reload_io.close()
