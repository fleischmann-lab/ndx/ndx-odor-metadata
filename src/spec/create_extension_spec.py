# -*- coding: utf-8 -*-
import os.path

import numpy as np
from pynwb.spec import (
    NWBAttributeSpec,
    NWBDatasetSpec,
    NWBGroupSpec,
    NWBNamespaceBuilder,
    export_spec,
)


def main():
    # define version and write to file
    VERSION = "0.1.1"
    version_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "pynwb",
            "ndx_odor_metadata",
            "VERSION",
        )
    )
    with open(version_path, "w") as f:
        f.write(VERSION)

    # define metadata
    ns_builder = NWBNamespaceBuilder(
        doc="""NWB Extension for documenting odor stimulus metadata""",
        name="""ndx-odor-metadata""",
        version=VERSION,
        author=["Tuan Pham"],
        contact=["tuanhpham@brown.edu"],
    )

    ns_builder.include_type("DynamicTable", namespace="hdmf-common")
    ns_builder.include_type("VectorData", namespace="hdmf-common")

    val_NA = "N/A"

    odor_table = NWBGroupSpec(
        default_name="odor_table",
        neurodata_type_def="OdorMetaData",
        neurodata_type_inc="DynamicTable",
        doc="A table for odor stimulus metadata. This is mainly for odor/chemical stimuli, '\
            'but can also include non-chemical stimuli in addition.",
        attributes=[
            NWBAttributeSpec(
                name="description",
                doc="Description of this odor stimulus metadata table",
                dtype="text",
                default_value="Odor stimulus metadata table",
                required=True,
            ),
            NWBAttributeSpec(
                name="comments",
                doc="Additional comments and notes about the table if needed",
                dtype="text",
                required=False,
                default_value="",
            ),
        ],
        datasets=[
            NWBDatasetSpec(
                name="stim_name",
                doc='Stimulus name, e.g. "hexanal" or "sound"',
                dtype="text",
                neurodata_type_inc="VectorData",
            ),
            NWBDatasetSpec(
                name="pubchem_id",
                doc="PubChem ID, `NaN` indicates non-(standard)-odor stimulus",
                dtype="float",
                neurodata_type_inc="VectorData",
                default_value=np.nan,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="raw_id",
                doc="Raw acquisition stimulus ID. Will be converted to `str`.",
                dtype="text",
                neurodata_type_inc="VectorData",
            ),
            NWBDatasetSpec(
                name="raw_id_dtype",
                doc="The actual dtype of `raw_id` value. Useful for (re)casting.",
                dtype="text",
                neurodata_type_inc="VectorData",
                default_value=val_NA,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="stim_id",
                doc="Preferred stimulus ID, which can be used to "
                "remap acquisition stimulus id `raw_id`. "
                "Will be converted to `str`. "
                "If not explicitly given, will copy from `raw_id`",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
            ),
            NWBDatasetSpec(
                name="stim_id_dtype",
                doc="The actual dtype of `stim_id` value. Useful for (re)casting.",
                dtype="text",
                neurodata_type_inc="VectorData",
                default_value=val_NA,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="stim_types",
                doc="Type(s) of stimulus, e.g. 'odor', 'sound', 'control', 'CS', 'US', ...",
                dtype="text",
                neurodata_type_inc="VectorData",
            ),
            NWBDatasetSpec(
                name="stim_types_index",
                doc="Index for `stim_types`",
                neurodata_type_inc="VectorIndex",
            ),
            NWBDatasetSpec(
                name="stim_description",
                doc="Human-readable description, notes, comments of each stimulus",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="chemical_dilution_type",
                doc="Type of dilution, e.g. 'volume/volume', 'vaporized'",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="chemical_concentration",
                doc="Concentration of chemical",
                dtype="float",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=np.nan,
            ),
            NWBDatasetSpec(
                name="chemical_concentration_unit",
                doc='Unit of concentration, e.g. "%" or "M"',
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="chemical_solvent",
                doc="Solvent to dilute the chemicals in, e.g. 'mineral oil'",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="chemical_provider",
                doc="Provider of the chemicals, e.g. 'Sigma'",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="nonchemical_details",
                doc="Information about non-chemical/odor stimulus, e.g. 'sound' frequencies",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="is_validated",
                doc="Whether the stimulus, if chemical/odor, "
                "is validated against PubChem (or other sources listed in `validation_info`."
                "If does not have a valid PubChem ID, this assumes to default `False` value",
                dtype="bool",
                neurodata_type_inc="VectorData",
                default_value=False,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="validation_details",
                doc="Additional information/details/notes about stimulus validation, "
                "e.g. source, software used & version, validation date, ...",
                dtype="text",
                neurodata_type_inc="VectorData",
                quantity="?",
                default_value=val_NA,
            ),
            NWBDatasetSpec(
                name="pubchem_cid",
                doc="PubChem CID, `NaN` indicates non-(standard)-odor stimulus",
                dtype="float",
                neurodata_type_inc="VectorData",
                default_value=np.nan,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="chemical_IUPAC",
                doc="Official chemical IUPAC name",
                dtype="text",
                neurodata_type_inc="VectorData",
                default_value=val_NA,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="chemical_SMILES",
                doc="Canonical SMILES",
                dtype="text",
                neurodata_type_inc="VectorData",
                default_value=val_NA,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="chemical_synonyms",
                doc="List of chemical synonyms",
                dtype="text",
                neurodata_type_inc="VectorData",
                default_value="",
                quantity="?",
            ),
            NWBDatasetSpec(
                name="chemical_synonyms_index",
                doc="Index for `chemical_synonyms`",
                neurodata_type_inc="VectorIndex",
                quantity="?",
            ),
            NWBDatasetSpec(
                name="chemical_molecular_formula",
                doc="Molecular formula of chemical used",
                dtype="text",
                neurodata_type_inc="VectorData",
                default_value=val_NA,
                quantity="?",
            ),
            NWBDatasetSpec(
                name="chemical_molecular_weight",
                doc="Molecular weight of chemical used",
                dtype="float",
                neurodata_type_inc="VectorData",
                default_value=np.nan,
                quantity="?",
            ),
        ],
    )

    new_data_types = [odor_table]

    # export the spec to yaml files in the spec folder
    output_dir = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "..", "spec")
    )
    export_spec(ns_builder, new_data_types, output_dir)
    print(
        "Spec files generated. Please make sure to rerun `pip install .` to load the changes."
    )


if __name__ == "__main__":
    # usage: python create_extension_spec.py
    main()
